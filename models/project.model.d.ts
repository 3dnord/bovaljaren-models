/* eslint-disable @typescript-eslint/no-explicit-any */
import { ObjectContainerClientT, ViewClientT, StatusClientT, ObjectTypeClientT, PoiContainerClientT, TourCollectionItemT, VideoClientT, FileItemInCollectionT, FileCollectionT } from '../';
import { FileDataT, ImageCollectionItemT, VideoFileCollectionItemT } from './file.model';

export type BrandingT = 'default' | 'ssh' | 'obos' | 'karnhem' | 'vnb' | 'myresjohus' | 'husman' | 'dios' | 'unik' | 'birger' | 'kopparstaden' | undefined;
export type LeaseFormT = 'sale' | 'lease';
/* export type ProjectT = {
    [key: string]: any;
    id: number;
    name: string;
    image: string;
    created: Date;
    defaultObjectTypeId: number;
    footnote: string;
    statusSummary: {
        count: number;
        status: StatusT;
    }[];
    active: boolean;
    completed?: Date;
} */

export type ProjectConfigT = {
    shareProject?: boolean;
    shareObject?: boolean;
    favorites?: boolean;
    fullscreen?: boolean;
    onboarding?: boolean;
    forceFullscreen?: boolean;
    videoLabel?: string;
}

export type ProjectInteresFormT = {
    mail: string;
    body: string;
    subject: string;
};

export type ProjectDataT = {
    name: string;
    defaultObjectTypeId?: number;
    footnote?: string;
    domain?: string;
    externalProjectUrl?: string;
    leaseForm?: LeaseFormT;
    branding: BrandingT;
    publishBvUrl?: boolean;
    publishDate?: Date | null;
    active: boolean;
    primaryViewId?: number;
    solarStudyId?: number | null;
    onboardingText?: string;
    imageCollectionId?: number | null;
    sitemapFileId?: number;
    videoId?: number | null;
    iframeOneId?: number | null;
    iframeTwoId?: number | null;
    deactivatedTimestamp: Date | null;
    activatedTimestamp: Date | null;
    defaultStatusId?: number; // nytt
    config?: ProjectConfigT | null;
    interestForm: ProjectInteresFormT | null;
}

export type ProjectExportDataT = {
    statusSummary: {
        count: number;
        status: StatusClientT;
    }[];
    created: Date;
    name: string;
    branding: BrandingT;
    active: boolean;
    deactivatedTimestamp: Date | null;
    activatedTimestamp: Date | null;
    syncedTimestamp: Date | null;
    count: {
        video: number;
        sunstudies: number;
        objects: number;
        iframes: number;
    }
    analyticsSummary: {
        objects:{
            count: number;
            id: string;
            name: string;
            type: string;
            uniqueCount: number
        }[]
        project:{
            count: number;
            id: string;
            uniqueCount: number
        }[]
        views:{
            count: number;
            name: string;
            id: string;
            uniqueCount: number;
            action: string;
        }[]
    }
}

export type ProjectCollectionItemT = ProjectDataT & {
    [key: string]: any;
    id: number;
    image: string;
    primaryViewImageFile?: FileDataT;
    deactivatedTimestamp: Date | null;
    activatedTimestamp: Date | null;
    onboardingText?: string;
    solarStudyId?: number | null;
    imageCollectionId?: number | null;
    iframeOneId?: number | null;
    iframeTwoId?: number | null;
    statusSummary: {
        count: number;
        status: StatusClientT;
    }[];
    created: Date;
    videoId?: number | null;
    active: boolean;
    config?: ProjectConfigT | null;
    interestForm: ProjectInteresFormT | null;
}

export type ProjectClientT = {
    id: number;
    primaryViewId: number;
    domain?: string;
    externalProjectUrl?: string;
    onboardingText?: string;
    leaseForm?: LeaseFormT;
    footnote?: string;
    iframeOneId?: number | null;
    iframeTwoId?: number | null;
    solarStudyId?: number | null;
    solarStudy?: FileCollectionT | null;
    imageCollectionId?: number | null;
    imageCollection?: FileCollectionT | null;
    branding: BrandingT;
    config?: ProjectConfigT | null;
    activatedTimestamp: Date | null;
    interestForm: ProjectInteresFormT | null;
    name: string;
    videoId?: number | null;
    video?: FileItemInCollectionT | null;
    views: ViewClientT[];
    objectTypes: ObjectTypeClientT[];
    sitemapImage: ImageCollectionItemT;
    statuses: StatusClientT[];
    tours: TourCollectionItemT[];
    objects: ObjectContainerClientT[];
    poiContainers: PoiContainerClientT[];
}
