
import { PolygonT } from './polygon.model';


export type MappingCollectionItemT = {
    id: number;
    mappingEntityId: number | null;
    viewId: number;
    polycords: PolygonT;
}
export type MappingDataT = {
    mappingEntityId: number | null;
    viewId: number;
    polycords: PolygonT;
}


export type MarkerCollectionItemT = {
    id: number;
    mappingEntityId: number | null;
    viewId: number;
    relativePosition: {
        x: number;
        y: number;
    };
}
export type MarkerDataT ={
    mappingEntityId: number | null;
    viewId: number;
    relativePosition: {
        x: number;
        y: number;
    };

}

export type MappingStateT = {
  state: any;
}


export type MarkerStateT = {
    state: any;
    mappingEntityId: number | null;
  }

export type MarkerClientT = MarkerCollectionItemT & MarkerDataT & MarkerStateT;

export type MappingClientT = MappingCollectionItemT & MappingDataT & MappingStateT;
