/* eslint-disable @typescript-eslint/no-explicit-any */
import { FileClientT } from './file.model';
import { ObjectTypePropertyT } from './object-type.model';


export type ObjectDataNumberT = {
    propId: number;
    value: number | null;
}

export type ObjectDataStringT = {
    propId: number;
    value: string;
}

export type ObjectDataBooleanT = {
    propId: number;
    value: boolean | null;
}


export type ObjectDataT = ObjectDataNumberT | ObjectDataStringT | ObjectDataBooleanT;

export type ObjectContainerCollectionItemT = {
    [key: string]: any;
    id: number;
    name: string;
    typeName: string | null; // 'Radhus' | 'Villa' | 'Parkeringsplats' | ... Defaults to objectType name
    data: ObjectDataT[];
    objectTypeId: number;
    statusId: number | null;
    imageCollectionId?: number | null;
    documentCollectionId?: number | null;
    floorPlanCollectionId?: number | null;
    videoCollectionId?: number | null;
    syncGroupId?: number | null;
    externalSyncId?: string | null;
    tourId?: number | null;
    measurePlanId?: number | null;
    iframeOneId?: number | null;
    iframeTwoId?: number | null;
    externalUrl?: string | null;
    syncUp?: boolean;
    config: unknown; // object-local config
    statusConfigOverride: unknown; // status specific configs that need to be overridden on this object.
    active: boolean;
}

export type ObjectContainerDataT = {
    name: string;
    data: ObjectDataT[];
    config: unknown; // object-local config
    objectTypeId: number;
    statusId: number | null;
    externalUrl?: string | null;
    imageCollectionId?: number | null;
    documentCollectionId?: number | null;
    floorPlanCollectionId?: number | null;
    videoCollectionId?: number | null;
    measurePlanId?: number | null;
    iframeOneId?: number | null;
    iframeTwoId?: number | null;
    tourId?: number | null;
    statusConfigOverride: unknown; // status specific configs that need to be overridden on this object.
    syncGroupId?: number | null;
    externalSyncId?: string | null;
    syncUp?: boolean;
    active: boolean;
}
export type ObjectContainerClientDataT = ObjectDataT & ObjectTypePropertyT;

export type ObjectContainerClientT = Omit<ObjectContainerCollectionItemT, 'data'> & Omit<ObjectContainerDataT, 'data'> & {
    files: FileClientT[];
    data: ObjectContainerClientDataT[];
};
