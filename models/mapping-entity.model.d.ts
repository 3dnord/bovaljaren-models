export type MappingEntityT ={
    id: number;
    type: string;
    projectId: number;
}