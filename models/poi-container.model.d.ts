import { ImageCollectionItemT } from './file.model';

export type PoiContainerDataT = {
    title: string;
    text: string;
    imageId: number | null;
    image: ImageCollectionItemT | null;
}
export type PoiContainerCollectionItemT = {
    id: number;
    title: string;
    text: string;
    imageId: number | null;
    image: ImageCollectionItemT | null;
}

export type PoiContainerClientT = PoiContainerCollectionItemT & PoiContainerDataT;