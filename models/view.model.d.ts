
import { ImageCollectionItemT, FlagClientT, MappingClientT, MarkerClientT, FileCollectionT, FileItemInCollectionT } from '..';
import { MappingCollectionItemT, MarkerCollectionItemT } from './mapping-container.model';

export type SitemapMarkerT = {
    scale: number;
    rotation: number;
    relativePosition: {
        x: number;
        y: number;
    };
    fov: number;
}

export type ViewDataT = {
    active: boolean;
    name: string;
    publishDate?: Date | null;
    sitemapMarker: SitemapMarkerT | null;
    imageFileId: number | null;
    slideshowId: number | null;
    imageCollectionId?: number | null;
    tourId?: number | null;
    solarStudyId?: number | null;
    videoId?: number | null;
    config?: ViewConfig | null;
    iframeOneId?: number | null;
    iframeTwoId?: number | null;
}
export type ViewConfig = {
    videoLabel?: string | null;
}

export type ViewCollectionItemT = ViewDataT & {
    id: number;
    imageCollectionId?: number | null;
    image?: ImageCollectionItemT;
    config?: ViewConfig | null;
}

export type MappingCollectionT = {
    views: MappingCollectionItemT[];
    objects: MappingCollectionItemT[];
    slideshow: MappingCollectionItemT[];
};

export type MarkerCollectionT = {
    views: MarkerCollectionItemT[];
    objects: MarkerCollectionItemT[];
    slideshow: MarkerCollectionItemT[];
};
export type StatusPointerDataT = {
    mappingEntityId: number | null;
    viewId: number | null;
    hideLabel?: boolean;
    relativePosition: {
        top: number;
        left: number;
    } | null;
}


export type StatusPointerT = {
    id: number;
    mappingEntityId: number | null;
    viewId: number | null;
    hideLabel?: boolean;
    relativePosition: {
        top: number;
        left: number;
    } | null;
}

export type ViewClientT = ViewCollectionItemT & ViewDataT & {
    imageFile: ImageCollectionItemT;
    video: FileItemInCollectionT;
    solarStudyId?: number | null;
    solarStudy?: FileCollectionT | null;
    imageCollectionId?: number | null;
    imageCollection?: FileCollectionT | null;
    iframeOneId?: number | null;
    config?: ViewConfig | null;
    iframeTwoId?: number | null;
    mappings: {
        views: MappingClientT[];
        objects: MappingClientT[];
        slideshow: MappingClientT[];
        poiContainers: MappingClientT[];
    };
    markers: {
        views: MarkerClientT[];
        objects: MarkerClientT[];
        slideshow: MarkerClientT[];
        poiContainers: MarkerClientT[];
    };
    flags: {
        views: FlagClientT[];
        objects: FlagClientT[];
        slideshow: FlagClientT[];
        poiContainers: FlagClientT[];
        simple: FlagClientT[];
    };
    statusPoints: StatusPointerT[];
}
