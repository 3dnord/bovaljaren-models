export type TourCollectionItemT = {
    id: number;
    projectId: number;
    name: string;
    url: string;
    label: string;
}

export type TourDataT = {
    projectId: number;
    name: string;
    url: string;
    label: string;
}

export type TourClientT = TourCollectionItemT & TourDataT;