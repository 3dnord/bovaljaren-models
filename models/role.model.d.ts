import { UserT } from './user.model';

type RoleCollectionItemT ={
    id: number;
    name: string;
    superadmin: boolean;
    policies: {
        bovaljaren: {
            [key: string]: PolicyT | null
        } | null;
    }
}

type RoleDataT = {
    name: string;
    policies: {
        bovaljaren: {
            [key: string]: PolicyT | null
        } | null;
    }
}

type ActionT = 'create' |'read' | 'update' | 'delete';

type PolicyT = {
    policyTemplateName: string;
    policy: {
        action: ActionT[];
        resource: string[];
    }[];
}


type RoleWithUsersT = RoleCollectionItemT & {
    users: UserT[] | null;
}

export { RoleDataT, RoleCollectionItemT, RoleWithUsersT, PolicyT, ActionT };