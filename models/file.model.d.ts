import { TagT } from './tag.model';

export type ImageCollectionBaseT = {
    filename: string;
    projectId: number;
    caption: string | null;
    listName: string | null;
    tags?: TagT[];
}
export type ImageCollectionItemT = ImageCollectionBaseT & {
    type: 'image'|'image-floorplan';
    url: {
        original: string;
        reduced: string;
        small: string;
        medium: string;
        large: string;
    } | null;
}
export type DocumentCollectionItemT = ImageCollectionBaseT & {
    type: 'document';
    url: {
        original: string;
    } | null;
    caption: string | null;
}
export type VideoFileCollectionItemT = {
    type: 'video';
    url: {
        original: string;
    } | null;
    caption: string | null;
} & ImageCollectionBaseT;

//used in admin for altering file meta data.
export type FileMetaDataT = {
    caption: string | null;
    listName: string | null;
}

//used in backend for adding new files
export type FileDataT = ImageCollectionItemT | DocumentCollectionItemT | VideoFileCollectionItemT;

/**
 * If you have a switch statement for type, i.e 'document' or 'image'
 * the type will change under the different cases.
 *
 * example:
 *
 * if (file.type === 'document) {
 *  //file is now considered of type DocumentCollectionItem and can be used as such.
 * }
 */
export type FileCollectionItemT = (ImageCollectionItemT | DocumentCollectionItemT | VideoFileCollectionItemT) & {
    id: number;
}


export type FileClientT = FileCollectionItemT;

