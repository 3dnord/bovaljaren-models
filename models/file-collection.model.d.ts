import { FileCollectionItemT } from './file.model';

export type FileCollectionDataTypes = 'image' | 'floor-plan' | 'document' | 'video' | 'sunstudy'

export type FileCollectionDataT = {
    projectId: number;
    name: string;
    type: FileCollectionDataTypes;
}

export type FileCollectionT = FileCollectionDataT & {
    id: number;
    files: FileItemInCollectionT[];
    objectContainers: {
        id: number;
        name: string;
    }[];
}

export type FileCollectionFileT = {
    fileCollectionId: number;
    fileId: number;
    sortOrder: number;
}

export type FileItemInCollectionT = FileCollectionItemT & { sortOrder: number };

export type FileCollectionMappingEntityT = {
    fileCollectionId: number;
    mappingEntityId: number;
}
