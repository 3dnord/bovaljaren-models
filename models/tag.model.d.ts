export type TagDataT = {
    projectId: number;
    name: string;
}

export type TagT = TagDataT & {
    id: number;
}

export type TagFileT = {
    tagId: number;
    fileId: number;
}
