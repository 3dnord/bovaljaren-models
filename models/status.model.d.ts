
export type StatusCollectionItemT = {
    id: number;
    type: string;
    singular: string;
    plural: string;
}

export type StatusTemplateT = {
    name:string,
    statuses:StatusTemplateDataT[]
};

export type StatusDataT = {
    type: string;
    singular: string;
    plural: string;
}

export type StatusTemplateDataT ={
    type: string;
    singular: string;
    plural: string;
    defaultStatus?: boolean;

}
export type StatusClientT = StatusCollectionItemT & StatusDataT;
