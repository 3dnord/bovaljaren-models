import { FileCollectionItemT } from './file.model';

export type VideoCollectionItemT = {
    id: number;
    projectId: number;
    name: string;
    fileId: number | null;
    data?: Record<string, unknown>;
}

export type VideoDataT = {
    projectId: number;
    name: string;
    fileId: number | null;
    data?: Record<string, unknown>;
}

export type VideoClientT = VideoCollectionItemT & VideoDataT & { file: FileCollectionItemT };