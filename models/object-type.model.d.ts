
export type ObjectTypeCollectionItemT = {
    id: number;
    name: string;
    typeName: string;
    importConfig?: {
        cbt: {
            name: string;
            type: string;
            baseType: string;
            index: number;
            transformed: boolean;
            id: number;
        }[];
    }
    properties: ObjectTypePropertyCollectionItemT[];
}

export type ObjectTypeDataT = {
    name: string;
    typeName: string;
    importConfig?: {
        cbt: {
            name: string;
            type: string;
            baseType: string;
            index: number;
            transformed: boolean;
            id: number;
        }[];
    }
    properties: ObjectTypePropertyT[];
}

export type ObjectTypeDataTemplateT = {
    name: string;
    typeName: string;
    properties: ObjectTypePropertyTemplateT[];
}
export type ObjectTypePropertyCollectionItemT = { id: number; } & ObjectTypePropertyT;

export type ObjectTypePropertyT = {
    objectTypeId: number;
    sortOrder: number;
    name: string;
    type: string;
    baseType: string;
    ending: string;
}
export type ObjectTypePropertyTemplateT = {
    sortOrder: number;
    name: string;
    type: string;
    baseType: string;
    ending: string;
}

export type ObjectTypeClientT = ObjectTypeCollectionItemT & ObjectTypeDataT;
