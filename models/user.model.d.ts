import { RoleCollectionItemT } from './role.model';


export type UserT = {
    id: number;
    name: string;
    email: string;
    password: string;
    roles: RoleCollectionItemT[];
}

export type UserCollectionItemT = UserDataT & {
    id: number;
}

type UserLoginT = {
    email: string;
    password: string;
}

export type UserDataT = {
    name: string;
    email: string;
    password: string;
}

export type UserDataPasswordT = {
    id: number;
    email: string;
    password: string;
    newPassword: string;
}

export type UserUpdateDataT = {
    id: number;
    name: string;
    email: string;
}
