export type SyncGroupCollectionItemT = SyncGroupDataT & {
    id: number;
}

export type SyncGroupDataT = {
    name: string;
    externalSyncType: string | null;
    externalSyncId: string | null;
    publishBvUrl: boolean;
    projectId: number;
    syncedTimestamp?: Date | null;
    syncActive: boolean;
    showObjects: boolean;
    showObjectPrice: boolean;
}