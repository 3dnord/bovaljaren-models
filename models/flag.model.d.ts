export type FlagSpecsT = {
    relativePosition: {
        top: number;
        left: number;
    }
    double: boolean;
    height: number;
    width: number;
}

export type FlagDataT = {
    text: string | null;
    mappingEntityId: number | null;
    viewId: number;
    specs: FlagSpecsT;
}

export type FlagCollectionItemT = {
    id: number;
    text: string | null;
    mappingEntityId: number | null;
    viewId: number;
    specs: FlagSpecsT;
}

export type FlagClientT = FlagCollectionItemT & FlagDataT;