export type SolarStudyCollectionItemT = {
    id: number;
    projectId: number;
    name: string;
    data: Record<string, unknown>;
}

export type SolarStudyDataT = {
    projectId: number;
    name: string;
    url: string;
    data: Record<string, unknown>;
}

export type SolarStudyClientT = SolarStudyCollectionItemT & SolarStudyDataT;