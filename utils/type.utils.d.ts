

/**
 * Input: T1, T2 => type objects
 * Returns: New combined type object.
 *
 * Method that overrides the common types of T1 with those of T2.
 */

export type Override<T1, T2> = Omit<T1, keyof T2> & T2;



